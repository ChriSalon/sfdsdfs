import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestMessageService {
    @Mock
    Network network;
    MessageService messageService= new MessageService(network);



    @Test
    public void testSentMessage() {
        assertTrue("192.176.21.1",messageService.sendMessage("129.121.43.1","Lorem ipsum"));
    }

    @Test
    public void testFailedMessage1() {
        assertFalse("192.176.21.1",messageService.sendMessage("129.121.431","Lorem ipsum"));
    }

    @Test
    public void testFailedMessage2() {
        assertFalse("192.176.21.1",messageService.sendMessage("","Lorem ipsum"));
    }
}
