import java.net.Inet4Address;
import java.net.InetAddress;

public class Network {
    public boolean sendMessage(String ip, String message)
    {
        try{
            InetAddress address = Inet4Address.getByName(ip);
            boolean reachable = address.isReachable(10000);
            if (reachable) return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
