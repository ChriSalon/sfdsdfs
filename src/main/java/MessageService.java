import java.net.Inet4Address;
import java.net.InetAddress;

public class MessageService {
    private final Network network;

    public MessageService(Network network) {
        this.network=network;
    }

    public boolean sendMessage(String ip, String message)
    {
        try{
            int retry=2;
            InetAddress address = Inet4Address.getByName(ip);

            while (retry>=0)
            {
                boolean reachable = address.isReachable(10000);
                //Send message here
                if (reachable) return true;
                retry--;
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
